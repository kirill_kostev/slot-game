const _ = require('lodash');


function validateOnInteger(value) {
    return _.isInteger(value)
}

function validateOnIntegerRange(value) {
    return _.inRange(value, 1, 5001)
}


export {validateOnInteger, validateOnIntegerRange}