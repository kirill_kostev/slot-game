import React, {Component} from "react"


class SpinButton extends Component {
    handleClick = () => {
        const isSpinned = this.props.appProps.isSpinned;
        const balance = this.props.appProps.balance;
        if (!isSpinned && balance > 0) {
            this.props.setSpinStatus(true);
            this.props.setBalance(balance - 1);
            this.props.startSpinProcess();
        }
    };

    render() {
        return (
            <button onClick={this.handleClick}>Spin</button>
        )
    }
}

export default SpinButton