import React, {Component} from 'react'
import Reels from './Reels'
import Balance from './Balance'
import DebugArea from './DebugArea'
import PayTable from './PayTable'

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            balance: 0,
            spinCost: 1,
            winAmount: '',
            isSpinned: false,
            debug: {
                isDebugMode: false,
            }
        }
    };

    setBalance = (value) => {
        if (!this.state.isSpinned) {
            this.setState({balance: value});
        }
    };

    setWinAmount = (value) => {
        this.setState({winAmount: value})
    };


    setReels = (value) => {
        this.setState({reels: value})
    };

    setSpinStatus = (bool) => {
        this.setState({isSpinned: bool})
    };

    setDebugMode = (bool) => {
        this.setState({isDebugMode: bool})
    };

    render() {
        return (
            <React.Fragment>
               <Balance
                   balance = {this.state.balance}
                   setBalance = {this.setBalance}
               />
               <Reels
                    appProps = {this.state}
                    setSpinStatus = {this.setSpinStatus}
                    setBalance = {this.setBalance}
                    setWinAmount = {this.setWinAmount}
                    />
               <PayTable winAmount = {this.state.winAmount}/>
               <DebugArea
                   isDebugMode = {this.state.isDebugMode}
                   setDebugMode = {this.setDebugMode}
                   setReels = {this.setReels}/>
            </React.Fragment>
        );
    }
}


export default App