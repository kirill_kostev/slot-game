import React, {Component} from "react"
import SYMBOLS from '../enum/symbols';

class Reel extends Component {
    render() {
        return (
            <div>
                {this.props.reelValuesArray.map(value => {
                    const index = value;
                    return (
                    <div key={SYMBOLS[index].id}>
                        <img
                            src={SYMBOLS[index].value}
                            alt={SYMBOLS[index].name}/>
                    </div>
                    )
                })}
            </div>
        )
    }
}

export default Reel