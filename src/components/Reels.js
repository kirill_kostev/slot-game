import React, {Component} from "react"
import Reel from './Reel';
import SpinButton from "./SpinButton";
import SYMBOLS from '../enum/symbols';


class Reels extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reels: [
                { id: 0, name: 'firstReelResult', value: [2, 3, 4] },
                { id: 1, name: 'secondReelResult', value: [2, 3, 4] },
                { id: 2, name: 'thirdReelResult', value: [2, 3 ,4] }
                ]
            }
        };

    setNewReels = (reelIndex, newArray) => {
        let newReels = this.state.reels;
        newReels[reelIndex].value = newArray;
        this.setState({reels: newReels});
    };

    getRandomSymbolIndex = () => {
        return Math.floor(Math.random() * SYMBOLS.length)
    };

    startSpinProcess = () => {
        this.setDefaultReelsPosition();
        this.spinReel();
        setTimeout( () => {
            this.stopSpinProcess();
            this.validateResultOnWin();
            this.props.setSpinStatus(false);
        }, 2000)
    };

    stopSpinProcess = () => {
        //TODO
    };

    spinReel = () => {

    };

    validateResultOnWin = () => {
        //TODO
    };

    setDefaultReelsPosition = () => {
        const defaultArray = [0, 1, 2];
        this.state.reels.map(reel => this.setNewReels(reel.id, defaultArray));
    };

    render() {
        return (
            <div>
                {this.state.reels.map(reel => (
                    <Reel
                        key = {reel.id}
                        name = {reel.name}
                        reelValuesArray = {reel.value}
                    />
                    ))}
                <SpinButton
                    appProps = {this.props.appProps}
                    startSpinProcess = {this.startSpinProcess}
                    setSpinStatus = {this.props.setSpinStatus}
                    setBalance = {this.props.setBalance}
                />
            </div>
        )
    }
}


export default Reels