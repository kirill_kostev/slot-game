import React, {Component} from 'react'
import {validateOnInteger, validateOnIntegerRange} from "../functions/inputValidation";


class Balance extends Component{
    constructor(props) {
        super(props);
        this.state = {inputValue: ''};
    }

    handleChange = (event) => {
        this.setState({inputValue: event.target.value});
    };

    handleSubmit = (event) => {
        const value = parseInt(this.state.inputValue);
        if (validateOnInteger(value)) {
            if (validateOnIntegerRange(value)) {
                this.props.setBalance(value);
            } else alert('Integer should be in range from 1 to 5000');
        } else alert('Input should be an integer!');
        this.cleanInputValue();
        event.preventDefault();
    };

    cleanInputValue = () => {
        this.setState({inputValue: ''});
    };

    render() {
        return (
            <div>
                <div>Current balance: {this.props.balance}</div>
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            Insert balance:
                            <input type="text" value={this.state.inputValue} onChange={this.handleChange} />
                        </label>
                        <input type="submit" value="Submit" />
                    </form>
                <div>Input should be an integer in range from 1 to 5000</div>
            </div>
        )
    }
}


export default Balance